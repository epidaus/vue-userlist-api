import axios from 'axios';

const url = 'https://reqres.in/';

class DataService {

    async getAllUser(page) {
        try {
            return await axios.get(url + `api/users?page=` + page)
        } catch (error) {
            throw error
        }
    }

    async getUserById(id) {
        try {
            return await axios.get(url + `api/users/` + id)
        } catch (error) {
            throw error
        }
    }

    async updateUser(id, param) {
        try {
            return await axios.put(url + `api/users/` + id, param);
        } catch (error) {
            throw error
        }
    }

    async deleteUser(id) {
        try {
            return await axios.delete(url + `api/users/` + id);
        } catch (error) {
            throw error
        }
    }


}

export default DataService;