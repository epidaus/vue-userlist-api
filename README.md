
## Clone project to local
```
git clone git@gitlab.com:epidaus/vue-userlist-api.git
```

## Install dependencies
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Browse project
```
http://localhost:8081
```